const _ = require('lodash');
const locationService = require('../service/location.service');

module.exports = {
    async getAllLocations(request, h) {
        const locations = await locationService.getAll();
        return h.response(locations);
    },

    async getLocationById(request, h) {
        const locationId = request.params.id;
        const location = await locationService.findById(locationId);
        return h.response(location);
    },

    async createLocation(request, h) {
        const locationToCreate = request.payload;
        const location = await locationService.create(locationToCreate.townName);
        return h.response(_.pick(location, ['_id'])).code(201);
    },

    async deleteLocation(request, h) {
        const locationId = request.params.id;
        await locationService.delete(locationId);
        return h.response().code(200);
    }
};