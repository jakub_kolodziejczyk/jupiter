'use strict';

const Axios = require('axios');
const Boom = require('boom');
const _ = require('lodash');

class AirConditionClient {

    configAirConditionClient(properties) {
        this.findAllUrl = properties.airQualityClient.findAllUrl;
        this.sensorsUrl = properties.airQualityClient.sensorsUrl;
        this.sensorDataUrl = properties.airQualityClient.sensorDataUrl;
    }

    async getAllStations() {
        try {
            const allStations = await Axios.get(this.findAllUrl);

            if (allStations && allStations.data) {
                return allStations.data;
            } else {
                throw Boom.serverUnavailable("There was a problem getting stations from air condition API");
            }
        } catch (err) {
            if (!err.isBoom) {
                throw Boom.serverUnavailable("There was a problem getting stations from air condition API");
            }
            throw err;
        }
    }

    async getSensorsForStations(stationIds) {
        try {
            const stationsUrls = _.map(stationIds, id => this.sensorsUrl + id);
            let sensorsFromStations = await Axios.all(_.map(stationsUrls, su => Axios.get(su)));
            sensorsFromStations = _.map(sensorsFromStations, s => s.data);
            return _.flatten(sensorsFromStations);
        } catch (err) {
            throw Boom.serverUnavailable("There was a problem getting sensors from air condition API");
        }
    }

    async getSensorDataFromSensors(sensorIds) {
        try {
            const sensorUrls = _.map(sensorIds, id => this.sensorDataUrl + id);
            let sensorsResults = await Axios.all(_.map(sensorUrls, su => Axios.get(su)));
            return _.map(sensorsResults, sr => sr.data);
        } catch (err) {
            throw Boom.serverUnavailable("There was a problem getting sensor data from air condition API");
        }
    }
}

module.exports = new AirConditionClient();