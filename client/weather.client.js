'use strict';

const Axios = require('axios');
const Boom = require('boom');

class WeatherClient {

    configWeatherClient(properties) {
        this.weatherUrl = properties.weatherClient.weatherUrl;
        this.countryCode = properties.weatherClient.countryCode;
    }

    async getWeatherData(town) {
        try {
            const weatherData = await Axios.get(this.weatherUrl + encodeURIComponent(town) + this.countryCode);

            if (weatherData && weatherData.data) {
                return weatherData.data;
            } else {
                throw Boom.serverUnavailable("There was a problem getting weather from weather API");
            }
        } catch (err) {
            if (!err.isBoom) {
                throw Boom.serverUnavailable("There was a problem getting weather from weather API");
            }
            throw err;
        }
    }
}

module.exports = new WeatherClient();