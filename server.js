'use strict';

const Hapi = require('hapi');
const dbConfig = require('./config/dbConfig');
const airConditionClient = require('./client/airCondition.client');
const weatherClient = require('./client/weather.client');
const locationRouter = require('./route/location.routes');

class AppServer {
    constructor() {
        this.properties = require('./config/properties')[process.env.NODE_ENV];
        this.server = this._createServer();

        this._configDatabase();
        this._configClients();
        this._configRoutes();
    }

    async start() {
        await this.server.start();
        console.log('Server running at:', this.server.info.uri);

        process.on('unhandledRejection', (err) => {
            console.log(err);
        });

        return this.server;
    }

    async init() {
        await this.server.initialize();
        return this.server;
    }

    _createServer() {
        return Hapi.server({
            port: this.properties.port,
            host: this.properties.host
        });
    }

    _configDatabase() {
        dbConfig.configDatabase(this.properties);
    }

    _configClients() {
        airConditionClient.configAirConditionClient(this.properties);
        weatherClient.configWeatherClient(this.properties);
    }

    _configRoutes() {
        this.server.route(locationRouter);
    }
}

module.exports = new AppServer();