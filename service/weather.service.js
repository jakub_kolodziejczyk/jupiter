'use strict';

const _ = require('lodash');
const airConditionClient = require('../client/airCondition.client');
const weatherClient = require('../client/weather.client');

class WeatherService {

    async getWeatherDataByTownName(town) {
        const weatherData = await weatherClient.getWeatherData(town);
        return {
            weather: weatherData.weather[0].description,
            temperature: weatherData.main.temp,
            temperatureFeelsLike: weatherData.main.feels_like,
            windSpeed: weatherData.wind.speed
        };
    }

    async getAirConditionDataBySensorIds(sensorIds) {
        const sensorsResults = await airConditionClient.getSensorDataFromSensors(sensorIds);

        let airQualityResult = _.map(sensorsResults, sr => {
            const sensorValue = _.head(_.reject(sr.values, sr => _.isNil(sr.value)));
            if (sensorValue) {
                const sensorName = sr.key;
                const sensorResult = {};
                sensorResult[sensorName] = sensorValue.value;
                return sensorResult;
            } else {
                return {};
            }
        });
        return _.reduce(airQualityResult, _.merge);
    }
}

module.exports = new WeatherService();