'use strict';

const _ = require('lodash');
const Boom = require('boom');
const locationRepo = require('../repository/location.repository');
const Location = require('../model/location.model');
const weatherService = require('../service/weather.service');
const airConditionClient = require('../client/airCondition.client');

class LocationService {
    constructor() {
        this._expandLocationWithWeatherData = this._expandLocationWithWeatherData.bind(this);
    }

    async getAll() {
        try {
            let locations = await locationRepo.findAll();
            locations = await Promise.all(locations.map(this._expandLocationWithWeatherData));
            return _.map(locations, l => _.pick(l, ['_id', 'name', 'weatherData', 'airConditionData']));
        } catch (err) {
            throw Boom.serverUnavailable("There was an unexpected error while getting locations data");
        }
    }

    async findById(id) {
        try {
            let location = await locationRepo.findById(id);
            if (_.isNil(location)) {
                throw Boom.notFound("There is no location with id: " + id);
            }
            location = await this._expandLocationWithWeatherData(location);
            return _.pick(location, ['_id', 'name', 'weatherData', 'airConditionData']);
        } catch (err) {
            if (!err.isBoom) {
                throw Boom.serverUnavailable("There was an unexpected error while getting location data");
            }
            throw err;
        }
    }

    async create(townName) {
        const existingLocation = await locationRepo.findByName(townName);
        if (existingLocation.length) {
            throw Boom.badRequest("Location with name: " + townName + " already exists");
        }

        const location = new Location({ name: townName });
        await this._prepareSensorIdsForLocation(location);

        await locationRepo.save(location);
        return location;
    }

    async delete(id) {
        const location = await locationRepo.findById(id);
        if (location) {
            await locationRepo.delete(location);
        } else {
            throw Boom.notFound("There is no location with id: " + id);
        }
    }

    async _expandLocationWithWeatherData(location) {
        let weatherData = {};
        let airConditionData = {};

        try {
            weatherData = await weatherService.getWeatherDataByTownName(location.name);
        } catch (err) {
            weatherData = { error: 'There was a problem while getting weather data for this location' };
            console.log(err.message);
        }

        try {
            if (_.isEmpty(location.sensorIds)) {
                await this._prepareSensorIdsForLocation(location);
            }

            airConditionData = await weatherService.getAirConditionDataBySensorIds(location.toObject().sensorIds);

            if (_.isNil(airConditionData) || _.isEmpty(airConditionData)) {
                airConditionData = { error: 'There was a problem while getting air condition data for this location' };
            }
        } catch (err) {
            airConditionData = { error: 'There was a problem while getting air condition data for this location' };
            console.log(err.message);
        }

        location = location.toObject();

        return _.merge(location, { weatherData: weatherData }, { airConditionData: airConditionData });
    }

    async _prepareSensorIdsForLocation(location) {
        try {
            const townSensorIds = await this._getStationIdsForTown(location.name);
            location.updateSensorIds(townSensorIds);
        } catch (err) {
            console.log("Error while getting sensor data for town: " + location.name)
        }
    }

    async _getStationIdsForTown(townName) {
        const allStations = await airConditionClient.getAllStations();
        const stations = _.filter(allStations, d => d.city.name.toUpperCase() === townName.toUpperCase());

        if (stations.length) {
            const stationIds = _.map(stations, 'id');
            let sensorsFromStations = await airConditionClient.getSensorsForStations(stationIds);
            sensorsFromStations = _.uniqBy(sensorsFromStations, s => s.param.paramCode);

            return _.map(sensorsFromStations, 'id');
        } else {
            throw Boom.badRequest("There is no station for town: " + town);
        }
    }
}

module.exports = new LocationService();