'use strict';

const chai = require("chai");
const sinon = require("sinon");
const faker = require("faker");
const expect = chai.expect;
const Boom = require('boom');
const mongoose = require('mongoose');

const dbConfig = require('../config/dbConfig');
const airConditionClient = require('../client/airCondition.client');
const weatherClient = require('../client/weather.client');
const Location = require('../model/location.model');
const locationService = require('../service/location.service');

describe('Server', () => {
    let server;
    let hapiServer;

    before(() => {
        process.env.NODE_ENV = 'dev';
        sinon.stub(dbConfig);
        sinon.stub(airConditionClient);
        sinon.stub(weatherClient);

        server = require('../server');
    });

    beforeEach(async () => {
        hapiServer = await server.init();
    });

    const locationFromDbStub = new Location({
        id: mongoose.Types.ObjectId(),
        name: faker.address.city(),
        sensorIds:  [ faker.random.number(), faker.random.number(), faker.random.number() ]
    });

    const location1Stub = {
        _id: "5e49d554b683ce1090bcdf40",
        name: "Wrocław",
        weatherData: {
            weather: "few clouds",
            temperature: 3.09,
            temperatureFeelsLike: -2.74,
            windSpeed: 5.7
        },
        "airConditionData": {
            NO2: 20.3251,
            O3: 45.9849,
            SO2: 1.5268,
            C6H6: 0.54143,
            CO: 183.051,
            PM10: 4.56158
        }
    };

    const location2Stub = {
        _id: "5e49e08acdbc32123dcfb5a3",
        name: "Międzygórze",
        weatherData: {
            weather: "light intensity shower rain",
            temperature: 3.39,
            temperatureFeelsLike: -2.91,
            windSpeed: 6.7
        },
        "airConditionData": {
            "error": "There was a problem while getting air condition data for this location"
        }
    };

    const listResponseStub = [ location1Stub, location2Stub ];
    const id = '5e49d554b683ce1090bcdf40';
    const malformedId = '123';

    describe('Getting all locations', () => {
        it('should return 200 with list of locations', async () => {
            const locationServiceFake = sinon.fake.resolves([ location1Stub, location2Stub ]);
            sinon.replace(locationService, 'getAll', locationServiceFake);

            const injectOptions = {
                method: 'GET',
                url: '/locations'
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.calledOnce).to.be.true;

            expect(response.statusCode).to.equal(200);

            const payload = JSON.parse(response.payload || {});
            expect(payload).to.have.lengthOf(2);
            expect(payload).to.be.deep.equal(listResponseStub);
        });

        it('should return 503 with nice payload when Boom error while getting locations', async () => {
            const errorMessage = "There was an unexpected error while getting locations data";
            const locationServiceFake = sinon.fake.rejects(Boom.serverUnavailable(errorMessage));
            sinon.replace(locationService, 'getAll', locationServiceFake);

            const injectOptions = {
                method: 'GET',
                url: '/locations'
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.calledOnce).to.be.true;

            expect(response.statusCode).to.equal(503);

            const payload = JSON.parse(response.payload || {});
            expect(payload).to.be.have.property('statusCode', 503);
            expect(payload).to.be.have.property('error', 'Service Unavailable');
            expect(payload).to.be.have.property('message', errorMessage);
        });

        it('should return 500 when other error while getting locations', async () => {
            const errorMessage = "Some other error";

            const locationServiceFake = sinon.fake.rejects(errorMessage);
            sinon.replace(locationService, 'getAll', locationServiceFake);

            const injectOptions = {
                method: 'GET',
                url: '/locations'
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.calledOnce).to.be.true;

            expect(response.statusCode).to.equal(500);

            const payload = JSON.parse(response.payload || {});
            expect(payload).to.be.have.property('statusCode');
            expect(payload).to.be.have.property('error');
            expect(payload).to.be.have.property('message');
        });
    });

    describe('Getting location by ID', () => {
        it('should return 200 with location data', async () => {
            const locationServiceFake = sinon.fake.resolves(location1Stub);
            sinon.replace(locationService, 'findById', locationServiceFake);

            const injectOptions = {
                method: 'GET',
                url: '/locations/' + id
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.calledOnce).to.be.true;

            expect(response.statusCode).to.equal(200);

            const payload = JSON.parse(response.payload || {});
            expect(payload).to.be.deep.equal(location1Stub);
        });

        it('should return 400 when providing malformed id', async () => {
            const locationServiceFake = sinon.fake.resolves(location1Stub);
            sinon.replace(locationService, 'findById', locationServiceFake);

            const injectOptions = {
                method: 'GET',
                url: '/locations/' + malformedId
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.notCalled).to.be.true;

            expect(response.statusCode).to.equal(400);

            const payload = JSON.parse(response.payload || {});
            expect(payload).to.be.have.property('statusCode', 400);
            expect(payload).to.be.have.property('error', 'Bad Request');
            expect(payload).to.be.have.property('message', 'Invalid request params input');
        });

        it('should return 404 when trying to get location that does not exist', async () => {
            const errorMessage = "There is no location with id: " + id;

            const locationServiceFake = sinon.fake.rejects(Boom.notFound(errorMessage));
            sinon.replace(locationService, 'findById', locationServiceFake);

            const injectOptions = {
                method: 'GET',
                url: '/locations/' + id
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.calledOnce).to.be.true;

            expect(response.statusCode).to.equal(404);

            const payload = JSON.parse(response.payload || {});
            expect(payload).to.be.have.property('statusCode', 404);
            expect(payload).to.be.have.property('error', 'Not Found');
            expect(payload).to.be.have.property('message', errorMessage);
        });

        it('should return 503 when other Boom error while getting location', async () => {
            const errorMessage = "There was an unexpected error while getting location data";

            const locationServiceFake = sinon.fake.rejects(Boom.serverUnavailable(errorMessage));
            sinon.replace(locationService, 'findById', locationServiceFake);

            const injectOptions = {
                method: 'GET',
                url: '/locations/' + id
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.calledOnce).to.be.true;

            expect(response.statusCode).to.equal(503);

            const payload = JSON.parse(response.payload || {});
            expect(payload).to.be.have.property('statusCode', 503);
            expect(payload).to.be.have.property('error', 'Service Unavailable');
            expect(payload).to.be.have.property('message', errorMessage);
        });

        it('should return 500 when other error while getting location', async () => {
            const errorMessage = "Some other error";

            const locationServiceFake = sinon.fake.rejects(errorMessage);
            sinon.replace(locationService, 'findById', locationServiceFake);

            const injectOptions = {
                method: 'GET',
                url: '/locations/' + id
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.calledOnce).to.be.true;

            expect(response.statusCode).to.equal(500);

            const payload = JSON.parse(response.payload || {});
            expect(payload).to.be.have.property('statusCode');
            expect(payload).to.be.have.property('error');
            expect(payload).to.be.have.property('message');
        });
    });

    describe('Creating location', () => {
        it('should return 201 with location id when location was created', async () => {
            const townName = locationFromDbStub.name;

            const locationServiceFake = sinon.fake.resolves(locationFromDbStub);
            sinon.replace(locationService, 'create', locationServiceFake);

            const injectOptions = {
                method: 'POST',
                url: '/locations',
                payload: {
                    'townName': townName
                }
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.calledOnce).to.be.true;

            expect(response.statusCode).to.equal(201);

            const payload = JSON.parse(response.payload || {});
            expect(payload).to.be.have.property('_id', locationFromDbStub._id.toString());
        });

        it('should return 400 when trying to add location with less than 2 characters in name', async () => {
            const townName = 'A';

            const locationServiceFake = sinon.fake.resolves(locationFromDbStub);
            sinon.replace(locationService, 'create', locationServiceFake);

            const injectOptions = {
                method: 'POST',
                url: '/locations',
                payload: {
                    'townName': townName
                }
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.notCalled).to.be.true;

            expect(response.statusCode).to.equal(400);

            const payload = JSON.parse(response.payload || {});
            expect(payload).to.be.have.property('statusCode', 400);
            expect(payload).to.be.have.property('error', 'Bad Request');
            expect(payload).to.be.have.property('message', 'Invalid request payload input');
        });

        it('should return 400 when trying to add location with more than 15 characters in name', async () => {
            const townName = 'Looong town name';

            const locationServiceFake = sinon.fake.resolves(locationFromDbStub);
            sinon.replace(locationService, 'create', locationServiceFake);

            const injectOptions = {
                method: 'POST',
                url: '/locations',
                payload: {
                    'townName': townName
                }
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.notCalled).to.be.true;

            expect(response.statusCode).to.equal(400);

            const payload = JSON.parse(response.payload || {});
            expect(payload).to.be.have.property('statusCode', 400);
            expect(payload).to.be.have.property('error', 'Bad Request');
            expect(payload).to.be.have.property('message', 'Invalid request payload input');
        });

        it('should return 400 when trying to add location with name not as a string', async () => {
            const townName = 12345;

            const locationServiceFake = sinon.fake.resolves(locationFromDbStub);
            sinon.replace(locationService, 'create', locationServiceFake);

            const injectOptions = {
                method: 'POST',
                url: '/locations',
                payload: {
                    'townName': townName
                }
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.notCalled).to.be.true;

            expect(response.statusCode).to.equal(400);

            const payload = JSON.parse(response.payload || {});
            expect(payload).to.be.have.property('statusCode', 400);
            expect(payload).to.be.have.property('error', 'Bad Request');
            expect(payload).to.be.have.property('message', 'Invalid request payload input');
        });

        it('should return 500 when other error while adding a location', async () => {
            const townName = locationFromDbStub.name;
            const errorMessage = "Some other error";

            const locationServiceFake = sinon.fake.rejects(errorMessage);
            sinon.replace(locationService, 'create', locationServiceFake);

            const injectOptions = {
                method: 'POST',
                url: '/locations',
                payload: {
                    'townName': townName
                }
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.calledOnce).to.be.true;

            expect(response.statusCode).to.equal(500);

            const payload = JSON.parse(response.payload || {});
            expect(payload).to.be.have.property('statusCode');
            expect(payload).to.be.have.property('error');
            expect(payload).to.be.have.property('message');
        });
    });

    describe('Deleting location', () => {
        it('should return 200', async () => {
            const locationServiceFake = sinon.fake();
            sinon.replace(locationService, 'delete', locationServiceFake);

            const injectOptions = {
                method: 'DELETE',
                url: '/locations/' + id
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.calledOnceWith(id)).to.be.true;

            expect(response.statusCode).to.equal(200);
        });

        it('should return 400 when providing malformed id', async () => {
            const locationServiceFake = sinon.fake();
            sinon.replace(locationService, 'delete', locationServiceFake);

            const injectOptions = {
                method: 'DELETE',
                url: '/locations/' + malformedId
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.notCalled).to.be.true;

            expect(response.statusCode).to.equal(400);

            const payload = JSON.parse(response.payload || {});
            expect(payload).to.be.have.property('statusCode', 400);
            expect(payload).to.be.have.property('error', 'Bad Request');
            expect(payload).to.be.have.property('message', 'Invalid request params input');
        });

        it('should return 404 when trying to delete location that does not exist', async () => {
            const errorMessage = "There is no location with id: " + id;

            const locationServiceFake = sinon.fake.rejects(Boom.notFound(errorMessage));
            sinon.replace(locationService, 'delete', locationServiceFake);

            const injectOptions = {
                method: 'DELETE',
                url: '/locations/' + id
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.calledOnce).to.be.true;

            expect(response.statusCode).to.equal(404);

            const payload = JSON.parse(response.payload || {});
            expect(payload).to.be.have.property('statusCode', 404);
            expect(payload).to.be.have.property('error', 'Not Found');
            expect(payload).to.be.have.property('message', errorMessage);
        });

        it('should return 500 when other error while deleting location', async () => {
            const errorMessage = "Some other error";

            const locationServiceFake = sinon.fake.rejects(errorMessage);
            sinon.replace(locationService, 'delete', locationServiceFake);

            const injectOptions = {
                method: 'DELETE',
                url: '/locations/' + id
            };

            const response = await hapiServer.inject(injectOptions);

            expect(locationServiceFake.calledOnce).to.be.true;

            expect(response.statusCode).to.equal(500);

            const payload = JSON.parse(response.payload || {});
            expect(payload).to.be.have.property('statusCode');
            expect(payload).to.be.have.property('error');
            expect(payload).to.be.have.property('message');
        });
    });

    afterEach(() => {
        sinon.restore();
        hapiServer.stop();
    });

    after(() => {
        delete process.env.NODE_ENV;
    });
});