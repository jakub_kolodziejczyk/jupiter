'use strict';

const chai = require("chai");
const sinon = require("sinon");
const Axios = require('axios');
const expect = chai.expect;
const _ = require('lodash');

const airConditionClient = require('../../client/airCondition.client');

describe('AirConditionClient', () => {
    const properties = {
        airQualityClient: {
            findAllUrl: 'http://api.gios.gov.pl/pjp-api/rest/station/findAll',
            sensorsUrl: 'http://api.gios.gov.pl/pjp-api/rest/station/sensors/',
            sensorDataUrl: 'http://api.gios.gov.pl/pjp-api/rest/data/getData/'
        }
    };

    before(() => {
        airConditionClient.configAirConditionClient(properties);
    });

    describe('All stations API call', () => {
        it('should access air quality API and return all stations data', async () => {
            const apiResponseAllStationStub = {
                data: {
                    id: 11,
                    name: "Wrocław"
                }
            };

            const airConditionApiFake = sinon.fake.resolves(apiResponseAllStationStub);
            sinon.replace(Axios, 'get', airConditionApiFake);

            const result = await airConditionClient.getAllStations();

            expect(airConditionApiFake.calledOnce).to.be.true;

            expect(result).to.be.equal(apiResponseAllStationStub.data);
        });

        it('should throw 503 Boom error when getting empty result from API', async () => {
            const airConditionApiFake = sinon.fake.resolves({});
            sinon.replace(Axios, 'get', airConditionApiFake);

            try {
                await airConditionClient.getAllStations();
            } catch (err) {
                expect(airConditionApiFake.calledOnce).to.be.true;
                expect(err).to.have.property('isBoom', true);
                expect(err.output).to.have.property('statusCode', 503);
                return;
            }
            expect(false, 'API call has not thrown an error').to.be.ok;
        });

        it('should throw 503 Boom error when getting an error from API', async () => {
            const airConditionApiFake = sinon.fake.rejects("Error from API");
            sinon.replace(Axios, 'get', airConditionApiFake);

            try {
                await airConditionClient.getAllStations();
            } catch (err) {
                expect(airConditionApiFake.calledOnce).to.be.true;
                expect(err).to.have.property('isBoom', true);
                expect(err.output).to.have.property('statusCode', 503);
                return;
            }
            expect(false, 'API call has not thrown an error').to.be.ok;
        });
    });

    describe('Sensors for stations API call', () => {
        const stationIds = [ 11, 23 ];
        const apiUrls = _.map(stationIds, id => properties.airQualityClient.sensorsUrl + id);
        const apiResponseStub = [{
            data: [{
                    id: 92
                }, {
                    id: 29
                }]
            }, {
            data: [{
                    id: 81
                }, {
                    id: 18
                }]
            }];

        afterEach(() => {
            sinon.restore();
        });

        it('should access air quality API and return all sensors for stations', async () => {
            const airConditionApiSingleFake = sinon.fake.resolves();
            sinon.replace(Axios, 'get', airConditionApiSingleFake);

            const airConditionApiComboCallFake = sinon.fake.resolves(apiResponseStub);
            sinon.replace(Axios, 'all', airConditionApiComboCallFake);

            const result = await airConditionClient.getSensorsForStations(stationIds);

            expect(airConditionApiSingleFake.calledTwice).to.be.true;

            //check if correct URLs were constructed
            expect(airConditionApiSingleFake.calledWith(apiUrls[0])).to.be.true;
            expect(airConditionApiSingleFake.calledWith(apiUrls[1])).to.be.true;

            expect(airConditionApiComboCallFake.calledOnce).to.be.true;

            expect(result).to.be.deep.equal([
                {
                    "id": 92
                }, {
                    "id": 29
                }, {
                    "id": 81
                }, {
                    "id": 18
                }
            ]);
        });

        it('should throw 503 Boom error when getting an error from API call', async () => {
            const airConditionApiSingleFake = sinon.fake.resolves();
            sinon.replace(Axios, 'get', airConditionApiSingleFake);

            const airConditionApiComboCallFake = sinon.fake.rejects("Error from API");
            sinon.replace(Axios, 'all', airConditionApiComboCallFake);

            try {
                await airConditionClient.getSensorsForStations(stationIds);
            } catch (err) {
                expect(airConditionApiSingleFake.calledTwice).to.be.true;
                expect(airConditionApiComboCallFake.calledOnce).to.be.true;
                expect(err).to.have.property('isBoom', true);
                expect(err.output).to.have.property('statusCode', 503);
                return;
            }
            expect(false, 'API call has not thrown an error').to.be.ok;
        });
    });

    describe('Sensors data API call', () => {
        const sensorIds = [ 81, 93 ];
        const apiUrls = _.map(sensorIds, id => properties.airQualityClient.sensorDataUrl + id);
        const apiResponseStub = [{
            data: {
                "key": "PM10"
            }
        }, {
            data: {
                "key": "CO"
            }
        }];

        it('should access air quality API and return sensors data', async () => {
            const airConditionApiSingleFake = sinon.fake.resolves();
            sinon.replace(Axios, 'get', airConditionApiSingleFake);

            const airConditionApiComboCallFake = sinon.fake.resolves(apiResponseStub);
            sinon.replace(Axios, 'all', airConditionApiComboCallFake);

            const result = await airConditionClient.getSensorDataFromSensors(sensorIds);

            expect(airConditionApiSingleFake.calledTwice).to.be.true;

            //check if correct URLs were constructed
            expect(airConditionApiSingleFake.calledWith(apiUrls[0])).to.be.true;
            expect(airConditionApiSingleFake.calledWith(apiUrls[1])).to.be.true;

            expect(airConditionApiComboCallFake.calledOnce).to.be.true;

            expect(result).to.be.deep.equal([{
                "key": "PM10"
            }, {
                "key": "CO"
            }]);
        });

        it('should throw 503 Boom error when getting an error from API call', async () => {
            const airConditionApiSingleFake = sinon.fake.resolves();
            sinon.replace(Axios, 'get', airConditionApiSingleFake);

            const airConditionApiComboCallFake = sinon.fake.rejects("Error from API");
            sinon.replace(Axios, 'all', airConditionApiComboCallFake);

            try {
                await airConditionClient.getSensorDataFromSensors(sensorIds);
            } catch (err) {
                expect(airConditionApiSingleFake.calledTwice).to.be.true;
                expect(airConditionApiComboCallFake.calledOnce).to.be.true;
                expect(err).to.have.property('isBoom', true);
                expect(err.output).to.have.property('statusCode', 503);
                return;
            }
            expect(false, 'API call has not thrown an error').to.be.ok;
        });
    });

    afterEach(() => {
        sinon.restore();
    });
});