'use strict';

const chai = require("chai");
const sinon = require("sinon");
const Axios = require('axios');
const expect = chai.expect;

const weatherClient = require('../../client/weather.client');

describe('WeatherClient', () => {
    const townName = "Wrocław";
    const properties = {
        weatherClient: {
            weatherUrl: 'http://api.openweathermap.org/data/2.5/weather?units=metric&APPID=f73d28e492dac09281d87b9c199737ff&q=',
            countryCode: ",pl"
        }
    };
    const apiUrl = properties.weatherClient.weatherUrl + encodeURIComponent(townName) + properties.weatherClient.countryCode;

    before(() => {
        weatherClient.configWeatherClient(properties);
    });

    it('should access weather API return weather data', async () => {
        const apiResponseStub = {
            data: {
                weather: "sunny"
            }
        };

        const weatherApiFake = sinon.fake.resolves(apiResponseStub);
        sinon.replace(Axios, 'get', weatherApiFake);

        const result = await weatherClient.getWeatherData(townName);

        //check if correct URL is constructed
        expect(weatherApiFake.calledOnceWith(apiUrl)).to.be.true;

        expect(result).to.be.equal(apiResponseStub.data);
    });

    it('should throw 503 Boom error when getting empty result from API', async () => {
        const weatherApiFake = sinon.fake.resolves({});
        sinon.replace(Axios, 'get', weatherApiFake);

        try {
            await weatherClient.getWeatherData(townName);
        } catch (err) {
            expect(weatherApiFake.calledOnce).to.be.true;
            expect(err).to.have.property('isBoom', true);
            expect(err.output).to.have.property('statusCode', 503);
            return;
        }
        expect(false, 'API call has not thrown an error').to.be.ok;
    });

    it('should throw 503 Boom error when getting an error from API', async () => {
        const weatherApiFake = sinon.fake.rejects("Error from API");
        sinon.replace(Axios, 'get', weatherApiFake);

        try {
            await weatherClient.getWeatherData(townName);
        } catch (err) {
            expect(weatherApiFake.calledOnce).to.be.true;
            expect(err).to.have.property('isBoom', true);
            expect(err.output).to.have.property('statusCode', 503);
            return;
        }
        expect(false, 'API call has not thrown an error').to.be.ok;
    });

    afterEach(() => {
        sinon.restore();
    });
});