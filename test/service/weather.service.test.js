'use strict';

const chai = require("chai");
const sinon = require("sinon");
const expect = chai.expect;
const _ = require('lodash');

const airConditionClient = require('../../client/airCondition.client');
const weatherClient = require('../../client/weather.client');
const weatherService = require('../../service/weather.service');

describe('WeatherService', () => {
    describe("Weather data", () => {
       it('should return correctly formatted weather data', async () => {
           const clientResponseStub = {
               weather: [
                   {
                       description: "clear sky"
                   }
               ],
               main: {
                   temp: 10.52,
                   feels_like: 5.99
               },
               wind: {
                   speed: 0.47
               }
           };

           const weatherClientFake = sinon.fake.resolves(clientResponseStub);
           sinon.replace(weatherClient, 'getWeatherData', weatherClientFake);

           const result = await weatherService.getWeatherDataByTownName("Wrocław");

           expect(weatherClientFake.calledOnce).to.be.true;

           expect(result).to.be.deep.equal({
               weather: clientResponseStub.weather[0].description,
               temperature: clientResponseStub.main.temp,
               temperatureFeelsLike: clientResponseStub.main.feels_like,
               windSpeed: clientResponseStub.wind.speed
           })
       }) ;
    });

    describe("Air condition data", () => {
        it('should return correctly formatted air condition data', async () => {
            const clientResponseStub = [{
                key: "PM10",
                values: [
                    {
                        "date": "2017-03-28 11:00:00",
                        "value": 30
                    },
                    {
                        "date": "2017-03-28 10:00:00",
                        "value": 33
                    }
                ]
            }, {
                key: "CO",
                values: [
                    {
                        "date": "2017-03-28 11:00:00",
                        "value": 10
                    },
                    {
                        "date": "2017-03-28 10:00:00",
                        "value": 9
                    }
                ]
            }];

            const airConditionClientFake = sinon.fake.resolves(clientResponseStub);
            sinon.replace(airConditionClient, 'getSensorDataFromSensors', airConditionClientFake);

            const result = await weatherService.getAirConditionDataBySensorIds([]);

            expect(airConditionClientFake.calledOnce).to.be.true;

            expect(result).to.be.deep.equal({
                PM10: 30,
                CO: 10
            })
        });

        it('should return correctly formatted air condition data skipping sensor that does not have any values', async () => {
            const clientResponseStub = [{
                key: "PM10",
                values: [
                    {
                        "date": "2017-03-28 10:00:00",
                        "value": 33
                    }
                ]
            }, {
                key: "CO",
                values: [
                    {
                        "date": "2017-03-28 10:00:00",
                    }
                ]
            }, {
                key: "NO2",
                values: [
                    {
                        "date": "2017-03-28 10:00:00",
                        "value": 9
                    }
                ]
            }];

            const airConditionClientFake = sinon.fake.resolves(clientResponseStub);
            sinon.replace(airConditionClient, 'getSensorDataFromSensors', airConditionClientFake);

            const result = await weatherService.getAirConditionDataBySensorIds([]);

            expect(airConditionClientFake.calledOnce).to.be.true;

            expect(result).to.be.deep.equal({
                PM10: 33,
                NO2: 9
            })
        });
    });

    afterEach(() => {
        sinon.restore();
    });
});