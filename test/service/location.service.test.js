'use strict';

const chai = require("chai");
const sinon = require("sinon");
const faker = require("faker");
const mongoose = require('mongoose');
const expect = chai.expect;

const locationService = require('../../service/location.service');
const locationRepo = require('../../repository/location.repository');
const Location = require('../../model/location.model');
const weatherService = require('../../service/weather.service');
const airConditionClient = require('../../client/airCondition.client');

describe('LocationService', () => {
    const locationStub = new Location({
        id: mongoose.Types.ObjectId(),
        name: faker.address.city(),
        sensorIds:  [ faker.random.number(), faker.random.number(), faker.random.number() ]
    });

    const weatherStub = {
        weather: "light rain",
        temperature: 8.11,
        temperatureFeelsLike: 4.29,
        windSpeed: 3.1
    };

    const airConditionStub = {
        "NO2": 11.7322,
        "O3": 67.2698,
        "SO2": 2.70742,
        "C6H6": 0.5251,
        "CO": 163.376,
        "PM2.5": 4.55404,
        "PM10": 8.37531
    };

    const locationWithoutSensorIdsStub = new Location({
        id: mongoose.Types.ObjectId(),
        name: faker.address.city(),
        sensorIds:  []
    });

    const stationListStub = [
        {
            id: faker.random.number(),
            city: {
                name: locationWithoutSensorIdsStub.name
            }
        },
        {
            id: faker.random.number(),
            city: {
                name: locationWithoutSensorIdsStub.name
            }
        },
        {
            id: faker.random.number(),
            city: {
                name: faker.address.city()
            }
        }
    ];

    const sensorListStub = [
        {
            "id": faker.random.number(),
            "stationId": stationListStub[0].id,
            "param": {
                "paramCode": "PM10",
            }
        },
        {
            "id": faker.random.number(),
            "stationId": stationListStub[0].id,
            "param": {
                "paramCode": "NO2",
            }
        },
        {
            "id": faker.random.number(),
            "stationId": stationListStub[1].id,
            "param": {
                "paramCode": "PM10",
            }
        }
    ];

    describe('getAll', () => {
        it('should return list of locations', async () => {
            const location2Stub = new Location({
                id: mongoose.Types.ObjectId(),
                name: faker.address.city(),
                sensorIds:  [ faker.random.number(), faker.random.number(), faker.random.number() ]
            });

            const locationRepoFake = sinon.fake.resolves([ locationStub, location2Stub ]);
            sinon.replace(locationRepo, 'findAll', locationRepoFake);

            const weatherFunctionFake = sinon.fake.resolves(weatherStub);
            sinon.replace(weatherService, 'getWeatherDataByTownName', weatherFunctionFake);

            const airConditionFunctionFake = sinon.fake.resolves(airConditionStub);
            sinon.replace(weatherService, 'getAirConditionDataBySensorIds', airConditionFunctionFake);

            const result = await locationService.getAll();

            expect(locationRepoFake.calledOnce).to.be.true;
            expect(weatherFunctionFake.calledTwice).to.be.true;
            expect(airConditionFunctionFake.calledTwice).to.be.true;

            expect(result).to.have.lengthOf(2);
            expect(result[0]).to.have.deep.property('_id', locationStub._id);
            expect(result[0]).to.have.property('name', locationStub.name);
            expect(result[0]).to.have.property('weatherData');
            expect(result[0]).to.have.property('airConditionData');
            expect(result[0].weatherData).to.not.have.property('error');
            expect(result[0].airConditionData).to.not.have.property('error');
            expect(result[0].weatherData).to.be.deep.equal(weatherStub);
            expect(result[0].airConditionData).to.be.deep.equal(airConditionStub);
            expect(result[1]).to.have.deep.property('_id', location2Stub._id);
            expect(result[1]).to.have.property('name', location2Stub.name);
            expect(result[1]).to.have.property('weatherData');
            expect(result[1]).to.have.property('airConditionData');
            expect(result[1].weatherData).to.not.have.property('error');
            expect(result[1].airConditionData).to.not.have.property('error');
            expect(result[1].weatherData).to.be.deep.equal(weatherStub);
            expect(result[1].airConditionData).to.be.deep.equal(airConditionStub);
        });

        it('should throw correct Boom error when having problem with database', async () => {
            const locationRepoFake = sinon.fake.rejects("Database error");
            sinon.replace(locationRepo, 'findAll', locationRepoFake);

            try {
                await locationService.getAll();
            } catch (err) {
                expect(err).to.have.property('isBoom', true);
                expect(err.output).to.have.property('statusCode', 503);
                return;
            }
            expect(false, 'findAll has not thrown an error').to.be.ok;
        });
    });

    describe('findById', () => {
        it('should return location by id', async () => {
            const locationRepoFake = sinon.fake.resolves(locationStub);
            sinon.replace(locationRepo, 'findById', locationRepoFake);

            const weatherFunctionFake = sinon.fake.resolves(weatherStub);
            sinon.replace(weatherService, 'getWeatherDataByTownName', weatherFunctionFake);

            const airConditionFunctionFake = sinon.fake.resolves(airConditionStub);
            sinon.replace(weatherService, 'getAirConditionDataBySensorIds', airConditionFunctionFake);

            const result = await locationService.findById(locationStub._id.str);

            expect(locationRepoFake.calledOnce).to.be.true;
            expect(weatherFunctionFake.calledOnce).to.be.true;
            expect(airConditionFunctionFake.calledOnce).to.be.true;

            expect(result).to.have.deep.property('_id', locationStub._id);
            expect(result).to.have.property('name', locationStub.name);
            expect(result).to.have.property('weatherData');
            expect(result).to.have.property('airConditionData');
            expect(result.weatherData).to.not.have.property('error');
            expect(result.airConditionData).to.not.have.property('error');
            expect(result.weatherData).to.be.deep.equal(weatherStub);
            expect(result.airConditionData).to.be.deep.equal(airConditionStub);
        });

        it('should throw correct Boom error when having problem with database', async () => {
            const locationRepoFake = sinon.fake.rejects("Database error");
            sinon.replace(locationRepo, 'findById', locationRepoFake);

            try {
                await locationService.findById(locationStub._id.str);
            } catch (err) {
                expect(err).to.have.property('isBoom', true);
                expect(err.output).to.have.property('statusCode', 503);
                return;
            }
            expect(false, 'findById has not thrown an error').to.be.ok;
        });

        it('should throw correct Boom 404 error when no location found in database', async () => {
            const locationRepoFake = sinon.fake.resolves(null);
            sinon.replace(locationRepo, 'findById', locationRepoFake);

            try {
                await locationService.findById(locationStub._id.str);
            } catch (err) {
                expect(err).to.have.property('isBoom', true);
                expect(err.output).to.have.property('statusCode', 404);
                return;
            }
            expect(false, 'findById has not thrown an error').to.be.ok;
        });

        it('should return location with correct weather message when getting an error from weather client', async () => {
            const locationRepoFake = sinon.fake.resolves(locationStub);
            sinon.replace(locationRepo, 'findById', locationRepoFake);

            const weatherFunctionFake = sinon.fake.rejects("Weather client error");
            sinon.replace(weatherService, 'getWeatherDataByTownName', weatherFunctionFake);

            const airConditionFunctionFake = sinon.fake.resolves(airConditionStub);
            sinon.replace(weatherService, 'getAirConditionDataBySensorIds', airConditionFunctionFake);

            const result = await locationService.findById(locationStub._id.str);

            expect(locationRepoFake.calledOnce).to.be.true;
            expect(weatherFunctionFake.calledOnce).to.be.true;
            expect(airConditionFunctionFake.calledOnce).to.be.true;

            expect(result).to.have.deep.property('_id', locationStub._id);
            expect(result).to.have.property('name', locationStub.name);
            expect(result).to.have.property('weatherData');
            expect(result).to.have.property('airConditionData');
            expect(result.weatherData).to.have.property('error');
            expect(result.airConditionData).to.not.have.property('error');
            expect(result.airConditionData).to.be.deep.equal(airConditionStub);
        });

        it('should return location with correct air condition message when getting an error from air condition client', async () => {
            const locationRepoFake = sinon.fake.resolves(locationStub);
            sinon.replace(locationRepo, 'findById', locationRepoFake);

            const weatherFunctionFake = sinon.fake.resolves(weatherStub);
            sinon.replace(weatherService, 'getWeatherDataByTownName', weatherFunctionFake);

            const airConditionFunctionFake = sinon.fake.rejects("Air condition client error");
            sinon.replace(weatherService, 'getAirConditionDataBySensorIds', airConditionFunctionFake);

            const result = await locationService.findById(locationStub._id.str);

            expect(locationRepoFake.calledOnce).to.be.true;
            expect(weatherFunctionFake.calledOnce).to.be.true;
            expect(airConditionFunctionFake.calledOnce).to.be.true;

            expect(result).to.have.deep.property('_id', locationStub._id);
            expect(result).to.have.property('name', locationStub.name);
            expect(result).to.have.property('weatherData');
            expect(result).to.have.property('airConditionData');
            expect(result.weatherData).to.not.have.property('error');
            expect(result.airConditionData).to.have.property('error');
            expect(result.weatherData).to.be.deep.equal(weatherStub);
        });

        it('should return location with correct air condition message when getting empty result from air condition client', async () => {
            const locationRepoFake = sinon.fake.resolves(locationStub);
            sinon.replace(locationRepo, 'findById', locationRepoFake);

            const weatherFunctionFake = sinon.fake.resolves(weatherStub);
            sinon.replace(weatherService, 'getWeatherDataByTownName', weatherFunctionFake);

            const airConditionFunctionFake = sinon.fake.resolves({});
            sinon.replace(weatherService, 'getAirConditionDataBySensorIds', airConditionFunctionFake);

            const result = await locationService.findById(locationStub._id.str);

            expect(locationRepoFake.calledOnce).to.be.true;
            expect(weatherFunctionFake.calledOnce).to.be.true;
            expect(airConditionFunctionFake.calledOnce).to.be.true;

            expect(result).to.have.deep.property('_id', locationStub._id);
            expect(result).to.have.property('name', locationStub.name);
            expect(result).to.have.property('weatherData');
            expect(result).to.have.property('airConditionData');
            expect(result.weatherData).to.not.have.property('error');
            expect(result.airConditionData).to.have.property('error');
            expect(result.weatherData).to.be.deep.equal(weatherStub);
        });

        it('should fill missing sensor IDs for location without them', async () => {
            const locationRepoFake = sinon.fake.resolves(locationWithoutSensorIdsStub);
            sinon.replace(locationRepo, 'findById', locationRepoFake);

            const weatherFunctionFake = sinon.fake.resolves(weatherStub);
            sinon.replace(weatherService, 'getWeatherDataByTownName', weatherFunctionFake);

            const airConditionFunctionFake = sinon.fake.resolves(airConditionStub);
            sinon.replace(weatherService, 'getAirConditionDataBySensorIds', airConditionFunctionFake);

            const allStationsFunctionFake = sinon.fake.resolves(stationListStub);
            sinon.replace(airConditionClient, 'getAllStations', allStationsFunctionFake);

            const sensorsFunctionFake = sinon.fake.resolves(sensorListStub);
            sinon.replace(airConditionClient, 'getSensorsForStations', sensorsFunctionFake);

            const result = await locationService.findById(locationWithoutSensorIdsStub._id.str);

            expect(locationRepoFake.calledOnce).to.be.true;
            expect(weatherFunctionFake.calledOnce).to.be.true;
            expect(allStationsFunctionFake.calledOnce).to.be.true;
            expect(sensorsFunctionFake.calledOnce).to.be.true;
            expect(airConditionFunctionFake.calledOnce).to.be.true;
            //check if we ended up having correct sensor IDs for location
            expect(airConditionFunctionFake.calledOnceWithExactly([sensorListStub[0].id, sensorListStub[1].id])).to.be.true;

            expect(result).to.have.deep.property('_id', locationWithoutSensorIdsStub._id);
            expect(result).to.have.property('name', locationWithoutSensorIdsStub.name);
            expect(result).to.have.property('weatherData');
            expect(result).to.have.property('airConditionData');
            expect(result.weatherData).to.not.have.property('error');
            expect(result.airConditionData).to.not.have.property('error');
            expect(result.weatherData).to.be.deep.equal(weatherStub);
            expect(result.airConditionData).to.be.deep.equal(airConditionStub);
        });
    });

    describe('create', () => {
        it('should create new location', async () => {
            const locationRepoFake = sinon.fake.resolves([]);
            sinon.replace(locationRepo, 'findByName', locationRepoFake);

            const allStationsFunctionFake = sinon.fake.resolves(stationListStub);
            sinon.replace(airConditionClient, 'getAllStations', allStationsFunctionFake);

            const sensorsFunctionFake = sinon.fake.resolves(sensorListStub);
            sinon.replace(airConditionClient, 'getSensorsForStations', sensorsFunctionFake);

            const locationSaveFunctionFake = sinon.fake.resolves();
            sinon.replace(locationRepo, 'save', locationSaveFunctionFake);

            const result = await locationService.create(locationWithoutSensorIdsStub.name);

            expect(locationRepoFake.calledOnce).to.be.true;
            expect(allStationsFunctionFake.calledOnce).to.be.true;
            expect(sensorsFunctionFake.calledOnceWithExactly([stationListStub[0].id, stationListStub[1].id])).to.be.true;
            expect(locationSaveFunctionFake.calledOnce).to.be.true;

            //checking if saved location has correct sensor IDs added
            expect(locationSaveFunctionFake.getCall(0).args[0]).to.have.property('name', locationWithoutSensorIdsStub.name);
            expect(locationSaveFunctionFake.getCall(0).args[0]).to.have.property('sensorIds').to.have.lengthOf(2);
            expect(locationSaveFunctionFake.getCall(0).args[0]).to.have.nested.property('sensorIds[0]', sensorListStub[0].id);
            expect(locationSaveFunctionFake.getCall(0).args[0]).to.have.nested.property('sensorIds[1]', sensorListStub[1].id);

            expect(result).to.have.deep.property('_id');
        });

        it('should throw correct Boom error when trying to add location that already exists', async () => {
            const locationFoundRepoFake = sinon.fake.resolves([locationWithoutSensorIdsStub]);
            sinon.replace(locationRepo, 'findByName', locationFoundRepoFake);

            try {
                await locationService.create(locationWithoutSensorIdsStub.name);
            } catch (err) {
                expect(err).to.have.property('isBoom', true);
                expect(err.output).to.have.property('statusCode', 400);
                return;
            }
            expect(false, 'create has not thrown an error').to.be.ok;
        });
    });

    describe('delete', () => {
        it('should delete location', async () => {
            const locationRepoFake = sinon.fake.resolves(locationStub);
            sinon.replace(locationRepo, 'findById', locationRepoFake);

            const locationDeleteFunctionFake = sinon.fake.resolves();
            sinon.replace(locationRepo, 'delete', locationDeleteFunctionFake);

            await locationService.delete(locationStub._id.str);

            expect(locationDeleteFunctionFake.calledOnceWith(locationStub)).to.be.true;
        });

        it('should throw 404 Boom error when trying to delete location that does not exist', async () => {
            const locationRepoFake = sinon.fake.resolves();
            sinon.replace(locationRepo, 'findById', locationRepoFake);

            const locationDeleteFunctionFake = sinon.fake.resolves();
            sinon.replace(locationRepo, 'delete', locationDeleteFunctionFake);

            try {
                await locationService.delete(locationStub._id.str);
            } catch (err) {
                expect(locationDeleteFunctionFake.notCalled).to.be.true;

                expect(err).to.have.property('isBoom', true);
                expect(err.output).to.have.property('statusCode', 404);
                return;
            }
            expect(false, 'delete has not thrown an error').to.be.ok;

        });
    });

    afterEach(() => {
        sinon.restore();
    });
});