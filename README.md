# README #

Job interview project

### How to run ###

`npm start` will start the app with dev properties

`npm start-dev` will start the app with dev properties using nodemon

`npm test` will run the tests

### Requirements and configuration

The app requires mongoDB started with database jupiter. Database connection string can be configured.

All configuration is placed in `/config/properties.js` file.
File was supposed to handle multiple profiles (like dev, test, prod, etc.) however now there is only config for dev environment.

### API ###

App presents a simple API with following methods:

`GET /locations`

Lists all added locations with current weather and air quality. Response example:
```
[
   {
       "_id": "5e49d554b683ce1090bcdf40",
       "name": "Wrocław",
       "weatherData": {
           "weather": "broken clouds",
           "temperature": 4.5,
           "temperatureFeelsLike": 0.9,
           "windSpeed": 2.6
       },
       "airConditionData": {
           "NO2": 7.20612,
           "O3": 51.3901,
           "SO2": 3.37037,
           "C6H6": 0.88796,
           "CO": 33.491600000000005,
           "PM2.5": 13.3528,
           "PM10": 18.5233
       }
   },
   {
       "_id": "5e49e034cdbc32123dcfb5a2",
       "name": "Konin",
       "weatherData": {
           "weather": "overcast clouds",
           "temperature": 3.33,
           "temperatureFeelsLike": -1.1,
           "windSpeed": 3.58
       },
       "airConditionData": {
           "CO": 289.16999999999996,
           "NO2": 9.78431,
           "O3": 18.614,
           "PM10": 29.178
       }
   }
]
```

`GET /locations/[locationId]`

`[locationId]` - ID of selected location

Presents location with provided ID with its current weather and air quality. Response example:
```
{
    "_id": "5e49d554b683ce1090bcdf40",
    "name": "Wrocław",
    "weatherData": {
        "weather": "broken clouds",
        "temperature": 4.5,
        "temperatureFeelsLike": 0.9,
        "windSpeed": 2.6
    },
    "airConditionData": {
        "NO2": 7.20612,
        "O3": 51.3901,
        "SO2": 3.37037,
        "C6H6": 0.88796,
        "CO": 33.491600000000005,
        "PM2.5": 13.3528,
        "PM10": 18.5233
    }
```

`POST /locations`

Creates new location. Request body is needed, e.g.:
```
{
    "townName": "Gdańsk"
}
```
`townName` must have between 2-15 characters.

As a response API returns 201 with ID of newly created location:
```
{
    "_id": "5e500ffdf25f5d30a6da9ad5"
}
```

`DELETE /locations/[locationId]`

`[locationId]` - ID of selected location

Deletes location with provided ID. As a response there is only 200 code.

### Security ###

Currently there is no security for the API. Unfortunately I run out of time... However I ws going to use things like:

https://github.com/auth0/node-jsonwebtoken

https://github.com/dwyl/hapi-auth-jwt2  
