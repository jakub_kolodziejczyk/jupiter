var mongoose = require('mongoose');

class DbConfig {

    configDatabase(properties) {
        mongoose.Promise = global.Promise;
        mongoose.connect(properties.db.connectionString, properties.db.mongooseConfig)
            .then(() => {
                console.log('Connected to database.');
            }).catch(err => {
                console.log('App starting error: ' + err.message);
                process.exit(1);
            });
    }
}

module.exports = new DbConfig();