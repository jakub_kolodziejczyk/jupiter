module.exports = {
    dev: {
        host: 'localhost',
        port: 3000,
        db: {
            connectionString: 'mongodb+srv://jupiter:jupiter@cluster0-acyjm.mongodb.net/jupiter?retryWrites=true&w=majority',
            mongooseConfig: {
                useNewUrlParser: true
            }
        },
        airQualityClient: {
            findAllUrl: 'http://api.gios.gov.pl/pjp-api/rest/station/findAll',
            sensorsUrl: 'http://api.gios.gov.pl/pjp-api/rest/station/sensors/',
            sensorDataUrl: 'http://api.gios.gov.pl/pjp-api/rest/data/getData/'
        },
        weatherClient: {
            weatherUrl: 'http://api.openweathermap.org/data/2.5/weather?units=metric&APPID=f73d28e492dac09281d87b9c199737ff&q=',
            countryCode: ",pl"
        }
    }
};