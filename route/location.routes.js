const locationHandler = require('../handler/location.handler');
const locationValidation = require('../validation/location.validation');

module.exports = [
    {
        method: 'GET',
        path: '/locations',
        handler: locationHandler.getAllLocations,
        options: {
            auth: false
        }
    },

    {
        method: 'GET',
        path: '/locations/{id}',
        handler: locationHandler.getLocationById,
        options: {
            auth: false,
            validate: locationValidation.validateGetLocationById
        }
    },

    {
        method: 'POST',
        path: '/locations',
        handler: locationHandler.createLocation,
        options: {
            auth: false,
            validate: locationValidation.validateCreateLocation
        }
    },

    {
        method: 'DELETE',
        path: '/locations/{id}',
        handler: locationHandler.deleteLocation,
        options: {
            auth: false,
            validate: locationValidation.validateDeleteLocation
        }
    },
];