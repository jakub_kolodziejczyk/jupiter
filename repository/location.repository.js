'use strict';

const Location = require('../model/location.model');

class LocationRepository {

    findById(id) {
        return Location.findById(id).exec();
    }

    findByName(townName) {
        return Location.find({
            name: { $regex : new RegExp('^' + townName + '$', "i") }
        }).exec();
    }

    findAll() {
        return Location.find().exec();
    }

    save(location) {
        return location.save();
    }

    delete(location) {
        return location.deleteOne({ _id: location._id });
    }
}

module.exports = new LocationRepository();