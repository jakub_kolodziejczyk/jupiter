'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Location = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 15
    },
    sensorIds: {
        type: [Number]
    }
});

class LocationClass {
    updateSensorIds(sensorIds) {
        this.sensorIds = sensorIds;
    }
}

Location.loadClass(LocationClass);

module.exports = mongoose.model('Location', Location);