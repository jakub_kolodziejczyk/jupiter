const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

module.exports = {
    validateGetLocationById: {
        params: {
            id: Joi.objectId()
        }
    },

    validateCreateLocation: {
        payload: Joi.object({
            townName: Joi.string().min(2).max(15),
        })
    },

    validateDeleteLocation: {
        params: {
            id: Joi.objectId()
        }
    }
};